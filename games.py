print()
print ("This code will be telling you a few things about some games I have and haven't played")

class game:
    Genre = ""
    AgeRating = 0
    MetacriticRating = 0 
    Perspective = ""
    Platform = ""
    IsItATripleAGame = False 

Cuphead = game()
Cuphead.Genre = "Platform Run and Gun"
Cuphead.AgeRating = 7
Cuphead.MetacriticRating = 86
Cuphead.Perspective = "2D" 
Cuphead.Platform = "Nintendo Switch, Playstation 4 + 5, Xbox one + Xbox Series and PC"
Cuphead.IsItATripleAGame = False 

print()
print("Cuphead can be played on the following platforms ", end = "")
print(Cuphead.Platform)
print()

ResidentEvil2Remake = game()
ResidentEvil2Remake.Genre = "SurvivalHorror"
ResidentEvil2Remake.AgeRating = 18 
ResidentEvil2Remake.MetacriticRating = 91
ResidentEvil2Remake.Perspective = "Third Person"
ResidentEvil2Remake.Platform = "Playstation 4 + 5, Xbox one + Xbox series, PC."
ResidentEvil2Remake.IsItATripleAGame = True

print("Resident evil 2 remake has a metacritic rating of ", end = "")
print(ResidentEvil2Remake.MetacriticRating, end="")
print(" out of 100")
print()

SilentHill2 = game()
SilentHill2.Genre = "Survival Horror"
#Lmao sorry, I will change up the Genres but I like Survival Horror games... Especially Silent Hill... 
SilentHill2.AgeRating = 18 
SilentHill2.MetacriticRating = 89
SilentHill2.Perspective = "Third Person"
SilentHill2.Platform = "Playstation 2 + 3, Xbox + Xbox 360 and PC."
SilentHill2.IsItATripleAGame = True 

print("Silent Hill 2 has a ", end = "")
print(SilentHill2.Perspective, end = "")
print(" perspective.")
print()

ClashOfClans = game()
ClashOfClans.Genre = "Online multiplayer Strategy game"
ClashOfClans.AgeRating = 13 
ClashOfClans.MetacriticRating = 74 
ClashOfClans.Perspective = "Top down isometric projection"
ClashOfClans.Platform = "Anroid and IOS"
ClashOfClans.IsItATripleAGame = True 

print("Clash of clans, the mobile game, has an age rating of ", end = "")
print(ClashOfClans.AgeRating, end = "")
print(".")
print()

BaldursGate3 = game()
BaldursGate3.Genre = "Strategy RPG"
BaldursGate3.AgeRating = 18 
BaldursGate3.MetacriticRating = 96
BaldursGate3.Perspective = "Third person"
BaldursGate3.Platform = "Playstation 5, Xbox series and PC"
BaldursGate3.IsItATripleAGame = True 

if (BaldursGate3.Genre == "Strategy RPG"):
    BaldursGate3.Genre = True 
else:
    BaldursGate3.Genre - False 
if BaldursGate3.Genre:
    print("Baldur's Gate 3 is a Strategy RPG Game.")
print()