print ("This Code is going to teach you about 5 different kinds of animals.")
print()

class animal:
    colour = ""
    legs = 0
    tail = ""
    skin = "" 
    isExtinct = False

cow = animal()
cow.colour = "Black and White"
cow.legs = 4
cow.tail = "Long with fur"
cow.skin = "Short fur" 
cow.isExtinct = False

print ("True or false, cows are extinct. ", end = "")
print (cow.isExtinct)
print ()

monkey = animal()
monkey.colour = "light brown"
monkey.legs = 2
monkey.tail = "long and skinny"
monkey.skin = "short to medium fur"
monkey.isExtinct = False

print ("The common monkey is ", end = "")
print (monkey.colour, end = "")
print (".")
print ()

dodo = animal()
dodo.colour = "White - grey"
dodo.legs = 2 
dodo.tail = "Dodo birds do not have tails"
dodo.skin = "medium - long feathers"
dodo.isExtinct = True

print ("true or false, dodo birds are extinct? ", end = "")
print (dodo.isExtinct, end = "")
print (".")
print ()

panda = animal()
panda.colour = "black and white"
panda.legs = 4
panda.tail = "short and stubby"
panda.skin = "short to medium soft fur"
panda.isExtinct = False 

print ("A panda's tail is ", end = "")
print (panda.tail, end = "")
print (".")
print ()

hedgehog = animal()
hedgehog.colour = "taupe"
hedgehog.legs = 4
hedgehog.tail = "short and stubby"
hedgehog.skin = "a mix of fur and quills"
hedgehog.isExtinct = False 

print ("a hedgehog has ", end = "")
print (hedgehog.legs, end = "")
print (" legs.")
print ()

print ("True or false, a dodo has ", end ="")

print (dodo.skin)
if (dodo.skin == "short feathers"):
    print ("false")
else:
    print ("true", end = "")

    print (".")