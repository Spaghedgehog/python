print()
print ("This code is made to tell you about the different features of my questionably favourite cars.")
print()
class car: 
    colour = ""
    wheels = 0 
    doors = 0 
    Transmission = ""
    smart = False 
    body = ""
    #Do i have to do this "layout" every time or was that so I could just copy paste it and make it easier? 

Fiat500 = car()
Fiat500.colour = "grey"
Fiat500.wheels = 4 
Fiat500.doors = 3 
Fiat500.Transmission = "Manual"
Fiat500.smart = False 
Fiat500.body = "Convertible"

print("The fiat 500 uses a ", end = "")
print (Fiat500.Transmission, end = "")
print(" transmission.")
print()

TradHearse = car()
TradHearse.colour = "black"
TradHearse.wheels = 4 
TradHearse.doors = 5
TradHearse.Transmission = "Automatic"
TradHearse.smart = False
TradHearse.body = "Long sided"

print("The Traditional Hearse has ", end = "")
print (TradHearse.doors, end = "")
print(" doors.")
print()
print("These next few cars aren't my favourite. Just some examples.")
print()

VolvoV40 = car()
VolvoV40.colour = "silver"
VolvoV40.wheels = 4
VolvoV40.doors = 5
VolvoV40.Transmission = "manual"
VolvoV40.smart = True
VolvoV40.body = "hatch back"

print("The Volvo V40's Body is a ", end = "")
print (VolvoV40.body, end = "")
print(".")
print()

ReliantRobinMk3 = car()
ReliantRobinMk3.colour = "Green"
ReliantRobinMk3.wheels = 3 
ReliantRobinMk3.doors = 2
ReliantRobinMk3.Transmission = "Manual"
ReliantRobinMk3.smart = False
ReliantRobinMk3.body = "Saloon"

print("True or false. Is the Reliant Robin MK3 a smart car? ", end = "")
print(ReliantRobinMk3.smart)
print()

Ioniq5 = car()
Ioniq5.colour = "Cyber Grey"
Ioniq5.wheels = 4 
Ioniq5.doors = 5
Ioniq5.Transmission = "Automatic"
Ioniq5.smart = True
Ioniq5.body = "Sport utility"

print("This Ioniq 5 in particular is a colour called ", end = "")
print(Ioniq5.colour, end = "")
print(".")
print()
print("That is everything for today about the cars of choice. Thanks for reading.")